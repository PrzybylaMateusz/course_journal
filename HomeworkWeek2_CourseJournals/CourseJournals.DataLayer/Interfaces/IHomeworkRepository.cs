﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseJournals.DataLayer.Models;

namespace CourseJournals.DataLayer.Interfaces
{
    public interface IHomeworkRepository
    {
        List<Homework> GetHomeWorkByCourseId(int id);
        List<HomeworkMarks> GetHomeWorkMarksByPesel(long pesel);
    }
}
