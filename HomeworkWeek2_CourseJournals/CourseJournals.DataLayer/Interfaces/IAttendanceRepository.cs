﻿using CourseJournals.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseJournals.DataLayer.Interfaces
{
    public interface IAttendanceRepository
    {
        List<ListOfPresent> GetListOfAllPresentsAtCourse(long pesel);
    }
}
