﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseJournals.DataLayer.Models;

namespace CourseJournals.DataLayer
{
    public interface ICoursesRepository
    {
      Course GetCoursesDataById(int id);
    }
}
