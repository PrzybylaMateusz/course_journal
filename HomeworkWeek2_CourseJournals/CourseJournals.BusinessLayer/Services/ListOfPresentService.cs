﻿using CourseJournals.BusinessLayer.Dtos;
using CourseJournals.BusinessLayer.Mappers;
using CourseJournals.DataLayer.Repositories;

namespace CourseJournals.BusinessLayer.Services
{
    public class ListOfPresentService
    {
        public bool AddListOfPresent(ListOfPresentDto listOfPresentDto)
        {
            var listOfPresent = DtoToEntityMapper.PresentDtoToPresent(listOfPresentDto);
            var listOfPresentRepository = new ListOfPresentRepository();
            return listOfPresentRepository.AddListOfPresent(listOfPresent);
        }
    }
}
