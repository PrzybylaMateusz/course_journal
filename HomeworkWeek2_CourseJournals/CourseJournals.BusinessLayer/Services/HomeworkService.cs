﻿using System;
using System.Collections.Generic;
using System.Linq;
using CourseJournals.BusinessLayer.Dtos;
using CourseJournals.BusinessLayer.Mappers;
using CourseJournals.DataLayer.Interfaces;
using CourseJournals.DataLayer.Repositories;

namespace CourseJournals.BusinessLayer.Services
{
    public class HomeworkService
    {
        private IHomeworkRepository _homeworkRepository;

        public HomeworkService(IHomeworkRepository homeworkRepository)
        {
            _homeworkRepository = homeworkRepository;
        }

        public HomeworkService()
        {
            _homeworkRepository = new HomeworkRepository();
        }

        public bool CheckIfTheHomeworkExists(string name)
        {
            var homeworkRepository = new HomeworkRepository();
            var homework = homeworkRepository.GetHomeWorkByName(name);
            return homework != null && homework.Count != 0;
        }

        public bool AddHomework(HomeworkDto homeworkDto)
        {
            var homework = DtoToEntityMapper.HomeworkDtotoHomework(homeworkDto);
            var homeworkRepository = new HomeworkRepository();
            return homeworkRepository.AddHomework(homework);
        }

        public List<HomeworkDto> GetListOfHomework(string courseId)
        {
            var homeworkRepository = new HomeworkRepository();
            var list = _homeworkRepository.GetHomeWorkByCourseId(Int32.Parse(courseId));
            return list.Select(record => EntityToDtoMapper.HomeworkEntityModelToDto(record)).ToList();
        }

        public double MaxPoints(List<HomeworkDto> listOfHomeworks)
        {
            double maxPoints = 0;
            foreach (var record in listOfHomeworks)
            {
                var i = record.MaxPoints;
                maxPoints = maxPoints + i;
            }
            return maxPoints;
        }

        public double CalculateStudentHomeworkPoints(List<HomeworkDto> listOfHomeworks, long pesel, List<HomeworkMarksDto> list)
        {
            double studentHomeworkPoints = 0;
            foreach (var homework in listOfHomeworks)
            {
                foreach (var record in list)
                {
                    if (record.Student.Pesel == pesel && record.HomeworkDto.Id == homework.Id)
                    {
                        studentHomeworkPoints = studentHomeworkPoints + record.HomeworkPoints;
                    }
                }
            }
            return studentHomeworkPoints;
        }
       
        public bool AddHomeworkMarks(HomeworkMarksDto homeworkMarksDto)
        {
            var homeworkMark = DtoToEntityMapper.HomeworkMarksDtotoHomeworkMarks(homeworkMarksDto);
            var homeworkRepository = new HomeworkRepository();
            return homeworkRepository.AddHomeworkMarks(homeworkMark);
        }

        public HomeworkDto GetHomeworkByName(string name, string id)
        {
            var homework = new HomeworkDto();
            var list = new HomeworkService().GetListOfHomework(id);
            foreach (var home in list)
            {
                if (home.HomeworkName == name)
                {
                    homework = home;
                }
            }
            return homework;
        }

        public List<HomeworkMarksDto> GetListOfHomeworkMarks(long pesel)
        {
            var listOfHomeworkMarks = new List<HomeworkMarksDto>();
            var homeworkRepository = new HomeworkRepository();
            var list = _homeworkRepository.GetHomeWorkMarksByPesel(pesel);
            foreach (var record in list)
            {
                var homeworkMarksDto = EntityToDtoMapper.HomeworkMarksEntityModelToDto(record);
                listOfHomeworkMarks.Add(homeworkMarksDto);
            }
            return listOfHomeworkMarks;
        }
    }
}
