﻿using System;
using System.Collections.Generic;
using CourseJournals.Cli.Helpers;
using CourseJournals.BusinessLayer.Dtos;
using CourseJournals.BusinessLayer.Services;

namespace CourseJournals.Cli
{
    internal class ProgramLoop
    {
        public void Execute()
        {
            var exit = false;
            while (!exit)
            {
                var choice = ConsoleReadHelper.GetCommand("Select number: \n");

                switch (choice)
                {
                    case "1":
                        AddStudents();
                        break;
                    case "2":
                        AddCourses();
                        break;
                    case "3":
                        ChoosingCourse();
                        break;
                    case "4":
                        PrintAllStudents();
                        break;
                    case "5":
                        EditStudentData();
                        break;
                    case "6":
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Value '" + choice + "' is invalid - try again");
                        break;
                }
            }
        }

        private void AddStudents()
        {
            Console.WriteLine("Provide information about the student:\n ");

            var student = new StudentDto();
            var studentService = new StudentService();

            var exit = false;
            while (!exit)
            {
                long pesel = ConsoleReadHelper.GetLong("Personal ID number: ");
                if (!studentService.CheckIfStudentsIsInTheDatabaseByPesel(pesel))
                {
                    student.Pesel = pesel;
                    exit = true;
                }
                else
                {
                    Console.WriteLine("The person with the given ID number is already in the datebase! Try again!");
                }
            }
            Console.Write("Student name: ");
            student.Name = Console.ReadLine();
            Console.Write("Student surname: ");
            student.Surname = Console.ReadLine();
            student.BirthDate = ConsoleReadHelper.GetDate("Student birthdate: (mm/dd/yyyy)");
            Console.Write("Student sex (Male/Female):");

            var correct = false;
            while (!correct)
            {
                var sex = Console.ReadLine();
                if (studentService.CheckIfSexIsCorrectValue(sex))
                {
                    student.Sex = sex;
                    correct = true;
                }
                else
                {
                    Console.WriteLine("Incorrect value - try again! (Male or Female);");
                }
            }

            var success = false;
            try
            {
                success = studentService.AddStudent(student);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error when adding student: " + e.Message);
            }
            ConsoleWriteHelper.PrintOperationSuccessMessage(success);
        }

        private void AddCourses()
        {
            Console.WriteLine("Provide informations about course: ");

            var course = new CourseDto();
            var courseService = new CourseService();
            var exit = false;
            while (!exit)
            {
                var courseId = ConsoleReadHelper.GetInt("Course ID:");
                if (!courseService.CheckIfCourseIsInTheDatabaseById(courseId))
                {
                    course.Id = courseId;
                    exit = true;
                }
                else
                {
                    Console.WriteLine("The given ID is already in the datebase. Try again!");
                }
            }
            Console.WriteLine("Course name: ");
            course.CourseName = Console.ReadLine();
            Console.WriteLine("Instructor name: ");
            course.InstructorName = Console.ReadLine();
            Console.WriteLine("Instructor surname: ");
            course.InstructorSurname = Console.ReadLine();
            course.StartDate = ConsoleReadHelper.GetDate("Start date: ");
            Console.Write("Minimum number of homework points (%): ");
            course.MinimalTresholdHomework = ConsoleReadHelper.GetValidNumberOfPoints();
            Console.WriteLine("Minimum number of attendance points (%): ");
            course.MinimalTresholdAttendance = ConsoleReadHelper.GetValidNumberOfPoints();
            course.NumbersOfStudents = ConsoleReadHelper.GetInt("Number of students: ");
            Console.WriteLine("Add students to the course:\n");

            course.Students = new List<StudentDto>();
            var exit1 = false;
            while (!exit1)
            {
                long pesel = ConsoleReadHelper.GetLong("\nGive student's ID number you want to add to the course: ");
                var studentService = new StudentService();
                if (!studentService.CheckIfStudentsIsInTheDatabaseByPesel(pesel))
                {
                    Console.WriteLine("\n No student with a given pesel in the database or incorrect data entered");
                }
                else
                {
                    if (course.Students.Exists(x => x.Pesel == pesel))
                    {
                        Console.WriteLine("\nA student with a given ID has been added to this course. Add another student.");
                    }
                    else
                    {
                        course.Students.Add(studentService.GetStudentData(pesel));
                        if (course.Students.Count == course.NumbersOfStudents)
                        {
                            Console.WriteLine("\nThe maximum number of students was reached!");
                            exit1 = true;
                        }
                    }
                }
            }

            var success = false;
            try
            {
                success = courseService.AddCourse(course);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error when adding course: " + e.Message);
            }
            ConsoleWriteHelper.PrintOperationSuccessMessage(success);
        }

        private void ChoosingCourse()
        {
            Console.WriteLine("List of available courses: ");
            var courseService = new CourseService();
            var allCourses = courseService.GetAllCoursesNames();
            if (courseService.GetAllCoursesNames().Count==0 || courseService.GetAllCoursesNames() == null)
            {
                Console.WriteLine("No courses added!");
            }
            else
            {
                string courseId = null;
                var exit = false;
                while (!exit)
                {
                    foreach (var course in allCourses)
                    {
                        Console.WriteLine(course.Key + " " + course.Value);
                    }
                    Console.Write("Select a course from the list above. Enter the course ID: ");
                    try
                    {
                        courseId = Console.ReadLine();
                        if (courseService.CheckIfCourseIsInTheDatabaseById(Int32.Parse(courseId)))
                        {
                            ConsoleWriteHelper.PrintCourseOperations(courseId);
                            exit = true;

                        }
                        else
                        {
                            Console.WriteLine("\nWrong course ID - try again!\n");
                        }
                    }
                    catch
                    {
                        Console.WriteLine("\nWrong course ID - try again!\n");
                    }
                }
                var exit1 = false;
                while (!exit1)
                {
                    string choose = Console.ReadLine();
                    switch (choose)
                    {
                        case "1":
                            CheckAttendance(courseId);
                            exit1 = true;
                            break;
                        case "2":
                            CheckHomework(courseId);
                            exit1 = true;
                            break;
                        case "3":
                            PrintReport(courseId);
                            exit1 = true;
                            break;
                        case "4":
                            EditCourseData(courseId);
                            exit1 = true;
                            break;
                        case "5":
                            exit1 = true;
                            break;
                        default:
                            Console.Write("Value '" + choose + "' is invalid - try again. Choose action: ");
                            break;
                    }
                }
            }
        }

        public void PrintAllStudents()
        {
            var studentService = new StudentService();
            var listAllStudents = studentService.GetAllStudentsList();
            foreach (var students in listAllStudents)
            {
                Console.WriteLine("\n" + students.Pesel + " " + students.Name + " " + students.Surname + "\n");
            }
        }

        public void CheckAttendance(string courseId)
        {
            var exit = false;
            while (!exit)
            {
                var date = ConsoleReadHelper.GetDate("Give a date to check attendance: ");
                var attendance = new AttendanceDto();
                var attendanceService = new AttendanceService();
                var listOfPresent = new ListOfPresentDto();
                var listOfPresentService = new ListOfPresentService();
                if (attendanceService.CheckIfDataIsInTheDatabase(date))
                {
                    Console.WriteLine("The given date was checked.");
                }
                else
                {
                    attendance.DayOfClass = date;
                    attendance.Courses = new CourseService().GetCourseDataById(courseId);
                    var success1 = false;
                    try
                    {
                        success1 = attendanceService.AddAttendance(attendance);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error when adding date: " + e.Message);
                    }
                    ConsoleWriteHelper.PrintOperationSuccessMessage(success1);
                    var studentService = new StudentService();
                    var studentsList = studentService.GetStudentsList(courseId);
                    foreach (var student in studentsList)
                    {
                        StudentDto studencik = student;
                        Console.WriteLine(student.Pesel + " " + student.Name + " " + student.Surname + ": ");
                        var correct = false;
                        while (!correct)
                        {
                            string present = Console.ReadLine();
                            if (attendanceService.CheckIfPresentIsCorrectValue(present))
                            {
                                listOfPresent.Attendance = attendanceService.Attendance(courseId, date);
                                listOfPresent.Student = studencik;
                                listOfPresent.Present = present;
                                correct = true;
                            }
                            else
                            {
                                Console.WriteLine("Incorrect value - try again! (Present or absent: ");
                            }
                        }
                        var success = false;
                        try
                        {
                            success = listOfPresentService.AddListOfPresent(listOfPresent);
                            exit = true;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error when adding student: " + e.Message);
                        }
                        ConsoleWriteHelper.PrintOperationSuccessMessage(success);
                    }
                }
            }
        }

        public void CheckHomework(string courseId)
        {
            Console.WriteLine("Give name of the homework or return to main menu('exit'):");
            var homework = new HomeworkDto();
            var exit = false;
            while (!exit)
            {
                var homeworkService = new HomeworkService();
                var nameOfHomework = Console.ReadLine();
                if (nameOfHomework == "exit")
                    return;
                else if (homeworkService.CheckIfTheHomeworkExists(nameOfHomework))
                {
                    Console.WriteLine("Homework with the given name exists. " +
                                      "Provide another name for homework or return to the main menu ('exit')");
                }
                else
                {
                    homework.HomeworkName = nameOfHomework;
                    var done = false;
                    while (!done)
                    {
                        try
                        {
                            Console.WriteLine("Maximum number of points: ");
                            homework.MaxPoints = double.Parse(Console.ReadLine());
                            done = true;
                        }
                        catch
                        {
                            Console.WriteLine("Incorrect value - try again!\n");
                        }
                    }
                    var courseService = new CourseService();
                    homework.Course = courseService.GetCourseDataById(courseId);
                    var success1 = false;
                    try
                    {
                        success1 = homeworkService.AddHomework(homework);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error when adding homework: " + e.Message);
                    }

                    ConsoleWriteHelper.PrintOperationSuccessMessage(success1);

                    var studentService = new StudentService();
                    var studentList = studentService.GetStudentsList(courseId);
                    foreach (var student in studentList)
                    {
                        var homeworkMarks =
                            new HomeworkMarksDto
                            {
                                HomeworkDto = homeworkService.GetHomeworkByName(nameOfHomework, courseId)
                            };
                        Console.WriteLine("\n" + student.Pesel + " " + student.Name + " " + student.Surname);
                        Console.WriteLine("Number of homework points by the student: ");
                        var working = false;
                        while (!working)
                        {
                            try
                            {
                                double homeworkPoints = double.Parse(Console.ReadLine());
                                if (homeworkPoints <= homework.MaxPoints && homeworkPoints >= 0)
                                {
                                    homeworkMarks.HomeworkPoints = homeworkPoints;
                                    working = true;
                                }
                                else
                                {
                                    Console.WriteLine("Number of points can not be greater " +
                                                      "than the maxium and must not be less than zero." +
                                                      " Try again.\nNumber of homework points" +
                                                      "by the student: ");
                                }
                            }
                            catch
                            {
                                Console.WriteLine("Wrong value - try again!");
                            }
                        }
                        homeworkMarks.Student = student;
                        var success = false;
                        try
                        {
                            success = homeworkService.AddHomeworkMarks(homeworkMarks);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Error when adding student: " + e.Message);
                        }

                        ConsoleWriteHelper.PrintOperationSuccessMessage(success);
                    }
                    exit = true;
                }
            }
        }
        public void PrintReport(string courseId)
        {
            var reportData = new CourseService().GetCourseDataById(courseId);
            Console.WriteLine("\nCourse ID: " + reportData.Id);
            Console.WriteLine("Course name: " + reportData.CourseName);
            Console.WriteLine("Instructor name: " + reportData.InstructorName);
            Console.WriteLine("Instructor surname: " + reportData.InstructorSurname);
            Console.WriteLine("Start date: " + reportData.StartDate);
            Console.WriteLine("Minimal treshold from attendance: " + reportData.MinimalTresholdAttendance);
            Console.WriteLine("Minimal treshold from homeworks: " + reportData.MinimalTresholdHomework);
            Console.WriteLine("Number of students: " + reportData.NumbersOfStudents);

            Console.WriteLine("Attendance results: ");
            var studentService = new StudentService();
            var studentsFromCourse = studentService.GetStudentsList(courseId);
            if (studentsFromCourse.Count != 0)
            {
                var attendanceService = new AttendanceService();
                var listOfAllDaysOfCourses = attendanceService.GetAttendanceList(courseId);
                double daysNumber = attendanceService.CountDaysNumber(listOfAllDaysOfCourses);

                if (daysNumber != 0)
                {
                    foreach (var student in studentsFromCourse)
                    {
                        var asd = new AttendanceService().GetMeListOfPresents(student.Pesel);

                        double numberOfPoints = attendanceService.AttendancePoints(student.Pesel, asd, listOfAllDaysOfCourses);

                        double procenteAttendance =
                            attendanceService.CalculateProcenteAttendance(daysNumber, numberOfPoints);
                        Console.WriteLine("\n" + student.Name + " " + student.Surname + " " + student.Pesel);
                        Console.WriteLine(numberOfPoints + "/" + daysNumber + "(" +
                                          procenteAttendance + "%)");
                        if (procenteAttendance > reportData.MinimalTresholdAttendance)
                        {
                            Console.WriteLine(" - Passed");
                        }
                        else
                        {
                            Console.WriteLine(" - Failed");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Attendance has not been checked. ");
                }
            }
            else
            {
                Console.WriteLine("No students assigned to the course");
            }

            Console.WriteLine("Homework results: ");
            if (studentsFromCourse.Count != 0)
            {
                var homeworkService = new HomeworkService();
                var listOfHomeworks = homeworkService.GetListOfHomework(courseId);
                var maxHomeworkPoints = homeworkService.MaxPoints(listOfHomeworks);

                if (maxHomeworkPoints != 0.0d)
                {
                    foreach (var student in studentsFromCourse)
                    {
                        var listOfHomeworksMarks = new HomeworkService().GetListOfHomeworkMarks(student.Pesel);
                        var studentsHomeworkPoints =
                            homeworkService.CalculateStudentHomeworkPoints(listOfHomeworks, student.Pesel, listOfHomeworksMarks);
                        double procentHomework = (studentsHomeworkPoints / maxHomeworkPoints) * 100;
                        Console.WriteLine("\n" + student.Name + " " + student.Surname + " " + student.Pesel);
                        Console.WriteLine(studentsHomeworkPoints + "/" + maxHomeworkPoints +
                                          " (" + procentHomework + ")");
                        if (procentHomework > reportData.MinimalTresholdHomework)
                        {
                            Console.WriteLine(" - Passed");
                        }
                        else
                        {
                            Console.WriteLine(" - Failed");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No homework has been checked!");
                }
            }
            else
            {
                Console.WriteLine("No students assigned to the course");
            }
        }

        public void EditCourseData(string id)
        {
            var courseService = new CourseService();
            var newCourseDto = new CourseDto();

            CourseDto courseDto = new CourseService().GetCourseDataById(id);
            Console.WriteLine("Enter current course data, or leave blank fields to keep the current data.\n");

            Console.Write("Course name: " + courseDto.CourseName + "   New course name: ");
            var newName = Console.ReadLine();
            newCourseDto.CourseName = string.IsNullOrEmpty(newName) ? courseDto.CourseName : newName;

            Console.Write("Instructor name: " + courseDto.InstructorName + "   New instructor name: ");
            var newInstructorName = Console.ReadLine();
            newCourseDto.InstructorName = string.IsNullOrEmpty(newInstructorName)
                ? courseDto.InstructorName
                : newInstructorName;

            Console.Write("Instructor surname: " + courseDto.InstructorSurname + "   New instructor surname: ");
            var newInstructorSurname = Console.ReadLine();
            newCourseDto.InstructorSurname = string.IsNullOrEmpty(newInstructorSurname) ? courseDto.InstructorSurname : newInstructorSurname;

            Console.Write("Minimum number of attendance points (%): " + courseDto.MinimalTresholdAttendance +
                "   New minimum number of attendance points (%): ");
            var newMinimalTresholdAttendance = Console.ReadLine();
            if (string.IsNullOrEmpty(newMinimalTresholdAttendance))
            {
                newCourseDto.MinimalTresholdAttendance = courseDto.MinimalTresholdAttendance;
            }
            else
            {
                newCourseDto.MinimalTresholdAttendance =
                    courseService.GetValidNumbersOfPoints(newMinimalTresholdAttendance);
            }

            Console.Write("Minimum number of homework points (%): " + courseDto.MinimalTresholdHomework +
                "   New minimum number of homework points (%): ");
            var newMinimalTresholdHomework = Console.ReadLine();
            if (string.IsNullOrEmpty(newMinimalTresholdHomework))
            {
                newCourseDto.MinimalTresholdHomework = courseDto.MinimalTresholdHomework;
            }
            else
            {
                newCourseDto.MinimalTresholdHomework =
                    courseService.GetValidNumbersOfPoints(newMinimalTresholdHomework);
            }
            var studentService = new StudentService();
            var exit = false;
            while (!exit)
            {
                Console.WriteLine("Remove selected student from the course. Give id or leave" +
                                  "blank to keep all current students or stop removing students: ");
                var studentToRemove = Console.ReadLine();
                var studentList = studentService.GetStudentsList(id);
                if (string.IsNullOrEmpty(studentToRemove))
                {
                    exit = true;
                }
                else if (studentList.Exists(s => s.Pesel == long.Parse(studentToRemove)))
                {
                    courseService.RemoveStudentFromCourse(id, long.Parse(studentToRemove));
                    if (new CourseService().GetCourseDataById(id).Students.Count != 0) continue;
                    Console.WriteLine("All students were removed from the course");
                    exit = true;
                }
                else
                {
                    Console.WriteLine("No student with given id on the list of students of the course!");
                }
            }
            newCourseDto.NumbersOfStudents = new CourseService().GetCourseDataById(id).Students.Count;
            courseService.ChangeCourseData(newCourseDto, id);
        }

        public void EditStudentData()
        {
            var studentService = new StudentService();
            var exit = false;
            while (!exit)
            {
                var studentPesel = ConsoleReadHelper.GetLong("Provide ID number to edit student data: ");
                if (!studentService.CheckIfStudentsIsInTheDatabaseByPesel(studentPesel))
                {
                    Console.WriteLine("\n No student with a given pesel in the database or incorrect data entered");
                }
                else
                {
                    StudentDto newStudentDto = new StudentDto();
                    StudentDto studentDto = studentService.GetStudentData(studentPesel);

                    Console.WriteLine("Enter current student data, or leave blank fields to keep the current data.\n");

                    newStudentDto.Pesel = studentDto.Pesel;

                    Console.Write("Student name: " + studentDto.Name + "   New student name: ");
                    var newName = Console.ReadLine();
                    newStudentDto.Name = string.IsNullOrEmpty(newName) ? studentDto.Name : newName;

                    Console.WriteLine("Student surname: " + studentDto.Surname + "   New student surname: ");
                    var newSurname = Console.ReadLine();
                    newStudentDto.Surname = string.IsNullOrEmpty(newSurname) ? studentDto.Surname : newSurname;

                    Console.WriteLine("Student birthdate: " +
                                      studentDto.BirthDate + "   New student birthdate (mm/dd/yyyy): ");
                    var newBirthdate = Console.ReadLine();
                    if (string.IsNullOrEmpty(newBirthdate))
                    {
                        newStudentDto.BirthDate = studentDto.BirthDate;
                    }
                    else
                    {
                        newStudentDto.BirthDate = studentService.GetDate(newBirthdate);
                    }
                    studentService.ChangeStudentData(newStudentDto);

                    var exit1 = false;
                    while (!exit1)
                    {
                        StudentDto studentDto2 = studentService.GetStudentData(studentPesel);
                        Console.WriteLine("List of student courses: \n");
                        foreach (var course in studentDto2.Courses)
                        {
                            Console.WriteLine(course.Id+" "+course.CourseName);
                        }
                        if (studentDto2.Courses.Count == 0)
                        {
                            Console.WriteLine("Student is not enrolled in any course!");
                            return;
                        }
                        Console.WriteLine("To remove a student from the course - " +
                                          "enter the course ID. Return to main menu - enter exit.");
                        var choose = Console.ReadLine();
                        try
                        {
                            if (choose == "exit")
                            {
                                exit = true;
                                exit1 = true;
                            }
                            else if (studentDto.Courses.Exists(s => s.Id == int.Parse(choose)))
                            {
                                var courseService = new CourseService();
                                courseService.RemoveStudentFromCourse(choose, studentDto.Pesel);
                                
                            }
                            else
                            {
                                Console.WriteLine("No course with given id!");
                            }
                        }
                        catch
                        {
                            Console.WriteLine("Value must be a number! Try again!");
                        }
                    }
                }
            }
        }
    }
}
